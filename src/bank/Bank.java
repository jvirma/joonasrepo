
package bank;






import java.util.ArrayList;


public class Bank {
    private ArrayList<Account> Tilit;  
    
    public Bank() {
        Tilit = new ArrayList<>();

    }    
    
    public void addTili(String s, int i) {
        TavanTili acc = new TavanTili();
        acc.tilinumero = s;
        acc.rahamaara = i;
        Tilit.add(acc);
        System.out.println("Tili luotu.");

    }
    public void addTili(String s, int i, int i2) {
        LuottoTili acc = new LuottoTili();
        acc.tilinumero = s;
        acc.rahamaara = i;
        acc.luottoraja = i2;
        Tilit.add(acc);
        System.out.println("Tili luotu.");

    }
    public Account getTili(String s) {
        for (Account c : Tilit) {
            if (c.tilinumero.equals(s)) {
                 System.out.println("Tilinumero: " + c.tilinumero + " Tilillä rahaa: " + c.rahamaara);
                 return c;
            }
        }
        return new LuottoTili();
           
    
    
    }
    public void nostaTili(String s, int i) {
        
        for (Account c : Tilit) {
            if (c.tilinumero.equals(s)) {
                 Class p = c.getClass();
                 if(i <= c.rahamaara && p.isInstance(c))
                    c.rahamaara = c.rahamaara - i;
                 
                 else if (c.rahamaara - i >= - c.luottoraja)
                    c.rahamaara = c.rahamaara - i;
                    
                 System.out.print("Tilinumero: " + c.tilinumero + " Tilillä rahaa: " + c.rahamaara);
                 if (c.rahamaara != 0)
                 System.out.print(" Luottoraja: " + c.luottoraja);
                 System.out.print("\n");

            }
        }
    
    }
    public void talletaTili(String s, int i) {
        for (Account c : Tilit) {
            if (c.tilinumero.equals(s)) {
                 c.rahamaara = c.rahamaara + i;

            }
        }
    }
    public void removeTili(String s) {
        for (Account c : Tilit) {
            if (c.tilinumero.equals(s) && !Tilit.isEmpty()) {
                 Tilit.remove(c);
                 break;
            }
        }
    
    }
    public void printAll() {
        for (Account c : Tilit) {
            System.out.print("Tilinumero: " + c.tilinumero + " Tilillä rahaa: " + c.rahamaara);
            if (c.luottoraja != 0)
            System.out.print(" Luottoraja: " + c.luottoraja);
            System.out.print("\n");
            }
        }
    }
    

    