package bank;


import java.util.Scanner;


public class Mainclass {
    public static void main(String[] args){
    Bank pankki = new Bank();
    while (true) {
    System.out.print("\n*** PANKKIJÄRJESTELMÄ ***\n1) Lisää tavallinen tili\n2) Lisää luotollinen tili\n3) Tallenna tilille rahaa\n4) Nosta tililtä\n5) Poista tili\n6) Tulosta tili\n7) Tulosta kaikki tilit\n0) Lopeta\nValintasi: ");
    Scanner scan = new Scanner(System.in);
    String temp = scan.next();
    int valinta = Integer.parseInt(temp);
    if (valinta == 0)
        break;
    else if (valinta == 1){
        System.out.print("Syötä tilinumero: ");
        String tilinumero = scan.next();
        System.out.print("Syötä rahamäärä: ");
        String tempRaha = scan.next();
        int rahamaara = Integer.parseInt(tempRaha);
        pankki.addTili(tilinumero, rahamaara);
        
    }
    else if (valinta == 2) {
        System.out.print("Syötä tilinumero: ");
        String tilinumero = scan.next();
        System.out.print("Syötä rahamäärä: ");
        String tempRaha = scan.next();
        int rahamaara = Integer.parseInt(tempRaha);
        System.out.print("Syötä luottoraja: ");
        String tempLuotto = scan.next();
        int luotto = Integer.parseInt(tempLuotto);
        pankki.addTili(tilinumero, rahamaara, luotto);
    }
    else if (valinta == 3){
        System.out.print("Syötä tilinumero: ");
        String tilinumero = scan.next();
        System.out.print("Syötä rahamäärä: ");
        String tempRaha = scan.next();
        int rahamaara = Integer.parseInt(tempRaha);
        pankki.talletaTili(tilinumero, rahamaara);
    }
    else if (valinta == 4){
        System.out.print("Syötä tilinumero: ");
        String tilinumero = scan.next();
        System.out.print("Syötä rahamäärä: ");
        String tempRaha = scan.next();
        int rahamaara = Integer.parseInt(tempRaha);
        pankki.nostaTili(tilinumero, rahamaara);
    }
    else if (valinta == 5){
        System.out.print("Syötä poistettava tilinumero: ");
        String tilinumero = scan.next();
        pankki.removeTili(tilinumero);
        System.out.println("Tili poistettu.");
    }
    else if (valinta == 6){
        System.out.print("Syötä tulostettava tilinumero: ");
        String tilinumero = scan.next();
        Account c = pankki.getTili(tilinumero);
    }
    else if (valinta == 7){

        System.out.println("Kaikki tilit:");
        pankki.printAll();
    }
    else
        System.out.println("Valinta ei kelpaa.");
    }
    }

}