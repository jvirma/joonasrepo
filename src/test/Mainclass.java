/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package test;

import java.util.Scanner;

/**
 *
 * @author m8582
 */
class Dog {
    public String name;
    public String lausahdus;
    
    public Dog() {
        name = "Mikko";
        lausahdus = "Juujuu";
    }
    
    public Dog(String s) {
        name = s;
    }
    public void speak() {
        System.out.println("Hei, minun nimeni on " + name + "!");
    };
    public void speakLause() {
        System.out.println(name + ":"+ lausahdus);
        if ("true".equals(lausahdus) || "false".equals(lausahdus)) {
            System.out.println(name + ": Annoit boolean arvon.");
        }
        if (isInteger(lausahdus)) {
            System.out.println(name + ": Annoit kok.luvun.");
        }
        
        
        
    };
    public static boolean isInteger(String s) {
        try {
            Integer.parseInt(s);
        } catch(NumberFormatException e) { 
        return false; 
        } catch(NullPointerException e) {
        return false;
        }
        return true;
        }
      }
   



public class Mainclass {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
     
        Dog Koira;
        Koira = new Dog();
        Scanner scan = new Scanner(System.in);
        String temp;
        System.out.print("Anna koiralle nimi: ");
        temp = scan.nextLine();
        temp = temp.trim();
        if(temp.isEmpty()) {
            Koira.name = "Doge";
        }
        else {
            Koira.name = temp;
        }
        System.out.print("Anna koiralle lausahdus: ");
        Koira.lausahdus = scan.next();

        Koira.speak();
        Koira.speakLause();
        
        Dog KoiraNimeton;
        KoiraNimeton = new Dog();
        KoiraNimeton.speak();
        KoiraNimeton.speakLause();
        
    }
    
}
